const Koa = require('koa');
const Router = require('@koa/router');
const sendfile = require('koa-sendfile');
const path = require('path');
const baseDir = path.dirname(__dirname);

const waiting = [];

module.exports = {
  start: (thumbnailsPath, images) => {
    const app = new Koa();
    const router = new Router();

    router.get('/thumbnails/:image', async (ctx, next) => {
      const stats = await sendfile(ctx, path.join(thumbnailsPath, ctx.params.image));
      if (!ctx.status) ctx.throw(404);
    });

    router.get('/images', (ctx, next) => {
      ctx.type = 'json';
      if (ctx.query.since) {
        const since = (new Date(ctx.request.query.since)).getTime();
        if (isNaN(since)) {
          ctx.throw(400);
          return;
        }

        const index = images.findIndex(
          (image) => (new Date(image.time)).getTime() > since
        );
        if (index === -1) {
          return new Promise((resolve, reject) => {
            waiting.push({
              ctx,
              resolve,
              reject,
              next,
              time: (new Date()).getTime()
            });
          });
        } else {
          ctx.body = images.slice(index);
          return next();
        }
      } else {
        ctx.body = images;
        return next();
      }
    });

    router.get('/', async (ctx, next) => {
      const stats = await sendfile(ctx, path.join(baseDir, 'app/index.html'));
      if (!ctx.status) ctx.throw(404);
    });

    router.get('/index.js', async (ctx, next) => {
      const stats = await sendfile(ctx, path.join(baseDir, 'app/index.js'));
      if (!ctx.status) ctx.throw(404);
      next();
    });

    app.use(router.routes()).use(router.allowedMethods());

    app.listen(80);
  },
  newImage: (image) => {
    if (waiting.length) {
      while (waiting.length) {
        const waited = waiting.shift();
        waited.ctx.body = [image];
        waited.resolve();
        waited.next();
      }
    }
  }
};
