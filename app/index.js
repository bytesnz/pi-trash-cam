const ul = document.getElementById('images');

const addImage = (image) => {
  const li = document.createElement('li');
  li.innerHTML = `<figure>
    <img src="thumbnails/${image.src}"${
      image.width ? ` width="${image.width}"` : ''
    }${
      image.height ? `height="${image.height}"` : ''
    }>
    <figcaption>
      <div>
        <strong>Time:</strong>
        <time datetime="image.time">${image.time}</time>
      </div>` + (image.position ? `
      <div>
        <strong>Position:</strong>
        ${image.position.lat.toFixed(6)}, ${image.position.lon.toFixed(6)}
      </div>` : '') + `
    </figcaption>
  </figure>`;

  ul.insertBefore(li, ul.firstChild);
}

let since = null;

const watch = () => {
  fetch(`images?since=${since}`).then((response) => {
    if (response.status === 200) {
      return response.json();
    }
  }, () => {}).then((newImages) => {
    if(newImages) {
      newImages.forEach(addImage);
      since = images.length ? images[images.length - 1].time : (new Date()).toISOString();
    }
    watch();
  });
};

fetch('images').then((response) => {
  if (response.status === 200) {
    return response.json();
  }
}).then((images) => {
  images.forEach(addImage);

  since = images.length ? images[images.length - 1].time : (new Date()).toISOString();
  watch();
});
