pi-trash-cam
============
Track and take photos of your picked litter

![First Image](https://bytes.nz/media/pi-trash-cam/first_image.jpg)

# Introduction

pi-trash-cam uses a
[Raspberry Pi Zero W](https://www.raspberrypi.org/products/raspberry-pi-zero-w/),
a [camera module](https://shop.pimoroni.com/products/raspberry-pi-zero-camera-module?variant=37751082058),
and GPS module like the
[NEO-6MV2 GPS module](https://components101.com/modules/neo-6mv2-gps-module) to
takes photos of and record the location of litter picked up by the litter
picker it is attached to.

It is written using Javascript and [Node.js](https://nodejs.org/en/) and can
run on
[Raspberry Pi OS Lite](https://www.raspberrypi.org/downloads/raspberry-pi-os/).

# To Use

1. Download the latest version of
  [Raspberry Pi OS Lite](https://www.raspberrypi.org/downloads/raspberry-pi-os/)
1. Image the microSD card you will use with the Pi with the downloaded image
  (instructions on how to do this are available from the same page as the
  download.
1. Add a *wpa_supplicant.conf* file to the
  boot partition on the microSD card with your WiFi details
  ```
  ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
  update_config=1

  network={
    ssid="WIFI_NAME"
    psk="WIFI_PASSWORD"
  }
  ```
1. Create an empty file called *ssh* on the boot partition to enabled
  [SSH](https://en.wikipedia.org/wiki/Secure_Shell).
1. Insert the microSD card into the Raspberry Pi and power it on
1. Log into the Pi over SSH (you can use
  [Putty](https://www.chiark.greenend.org.uk/~sgtatham/putty/) to do this
  on windows. The hostname will be *raspberrypi.local*, the username will
  be *pi* and the password *raspberry*.
1. Install the required software
  - *git* for downloading this repository
  - *Node.js* for running the software (check
    [the download page](https://nodejs.org/dist/latest-v10.x/) for the latest
    version (the file ending in *armv6l.tar.xz*)
  ```shell
  sudo apt update # Update the software registry
  sudo apt install git pigpio # Install Git and pigpio
  wget https://nodejs.org/dist/latest-v10.x/node-v10.22.0-linux-armv6l.tar.xz # Download Node
  sudo tar -xJf node-v10.22.0-linux-armv6l.tar.xz -C /opt/ # Extract node into /opt
  for f in `find /opt/node*/bin ! -type f`; do sudo ln -s $f /usr/local/bin; done # Add links to the Node executables
  ```
1. Clone this repository and set it up
  ```shell
  cd /opt # Change into the /opt directory
  sudo git clone https://gitlab.com/bytesnz/pi-trash-cam.git # Clone this repository
  cd pi-trash-cam # Change into the pi-trash-cam directory
  sudo npm install # Install pi-trash-cam dependencies
  sudo cp pi-trash-cam.service /etc/systemd/system # Install the service to start pi-trash-cam on boot
  sudo systemctl enable pi-trash-cam
  sudo mkdir -p /data/trash-images # Create a folder for the images
  ```
1. Reboot and pick that litter

Images will be stored in */data/trash-images*.

# Future Development

Plans include:
- automatic upload of images to somewhere (such as
  [Litterati](https://www.litterati.org/))
- installation/configuration helper
- WiFi hotspot to track litter as you pick

Feel free to
[create an issue](https://gitlab.com/bytesnz/pi-trash-cam/issues/new) or email
any ideas you may have.
