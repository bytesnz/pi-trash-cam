#!/usr/bin/env node
const file = '/dev/ttyS0';
const Gpio = require('pigpio').Gpio;
const { StillCamera, StreamCamera, Codec } = require("pi-camera-connect");
const fs = require('fs').promises;
const { R_OK, W_OK } = require('fs').constants;
const promisify = require('util').promisify;
const child_process = require('child_process');
const exec = promisify(child_process.exec);
const piexif = require('piexifjs');
const { join, resolve } = require('path');
const process = require('process');
const sharp = require('sharp');
const glob = require('fast-glob');

const server = require('./lib/server');

const GPS = require('gps');
const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline')
const port = new SerialPort(file, {
  baudRate: 9600,
  parser: new Readline({ delimiter: '\r\n' })
});
const wifi = process.env.WIRELESS_INTERFACE || 'wlan0';
const path = process.argv[2] || process.cwd();
const thumbnails = './thumbnails';
const thumbSize = 250;

const softwareId = 'pi-trash-cam';

console.log(`Using ${path} for images`);
const log = resolve(path, 'positions.log');
const thumbnailsPath = resolve(path, thumbnails);

const gps = new GPS;
let position = null;
let lastLogPosition = null;
let lastLogTime = null;
let updated = -1;

gps.on('data', function(data) {
  if (data.type === 'GLL' && data.valid && data.lat !== null && data.lon !== null && data.valid) {
    position = data;
    updated = (new Date()).getTime();
    // Check if position is 5m away from previous position or position hasn't been logged in the last minute
    if (
      !lastLogPosition ||
      GPS.Distance(lastLogPosition.lat, lastLogPosition.lon, data.lat, data.lon) * 1000 > 5 ||
      lastLogTime + 60000 < updated
    ) {
      fs.appendFile(log, `${data.time.toISOString()},${data.lat.toFixed(7)},${data.lon.toFixed(7)}\n`);
      lastLogPosition = position;
      lastLogTime = updated;
    }
  }
});

port.on('data', function(data) {
  gps.updatePartial(data);
});

const button = new Gpio(2, {
  mode: Gpio.INPUT,
  pullUpDown: Gpio.PUD_UP,
  edge: Gpio.FALLING_EDGE
});

const stillCamera = new StillCamera();

const zeroTen = (number) => number >= 10 ? number : '0' + number;
const exifDate = (date) => `${date.getFullYear()}:${
    zeroTen(date.getMonth() + 1)}:${zeroTen(date.getDate())} ${
    zeroTen(date.getHours())}:${zeroTen(date.getMinutes())}:${
    zeroTen(date.getSeconds())}`;

let images = [];

const takeImage = () => {
  let promise;

  if (true) {
    promise = stillCamera.takeImage();
  } else {
    const streamCamera = new StreamCamera({
        codec: Codec.MJPEG
    });

    promise = streamCamera.startCapture().then(() => streamCamera.takeImage())
        .then((image) => streamCamera.stopCapture().then(() => image));
  }

  return promise.then(async (image) => {
    // Create thumbnail
    const imageMetadata = await sharp(image).metadata();
    const thumbnail = await sharp(image).resize(thumbSize);
    const thumbnailBuffer = await thumbnail.toBuffer();
    const ratio = Math.min(thumbSize / imageMetadata.width, thumbSize / imageMetadata.height);
    const thumbnailMetadata = {
      width: imageMetadata.width * ratio,
      height: imageMetadata.height * ratio
    };
    console.log('meta', thumbnailMetadata);

    let exifImage, exifData;

    const time = (new Date());
    const metadata = {
      Exif: {
        [piexif.ExifIFD.DateTimeOriginal]: exifDate(time)
      },
      '0th': {
        [piexif.ImageIFD.XResolution]: thumbnailMetadata.width,
        [piexif.ImageIFD.YResolution]: thumbnailMetadata.height,
        [piexif.ImageIFD.Software]: softwareId
      }
    };
    let filename =  `${time.toISOString()}-no-pos.jpg`
    const serverImage = {
      src: filename,
      time: time.toISOString(),
      width: thumbnailMetadata.width,
      height: thumbnailMetadata.height
    };
    console.log('for server', serverImage);

    if (
      position && position.lat !== null && position.lon !== null &&
      updated > time.getTime() - (60*1000)
    ) {
      filename = `${time.toISOString()}.jpg`;
      serverImage.src = filename;
      serverImage.position = {
        lat: position.lat,
        lon: position.lon
      };
      Object.assign(metadata, {
        GPS: {
          [piexif.GPSIFD.GPSVersionID]: [7, 7, 7, 7],
          [piexif.GPSIFD.GPSDateStamp]: exifDate(time),
          [piexif.GPSIFD.GPSLatitudeRef]: position.lat < 0 ? 'S' : 'N',
          [piexif.GPSIFD.GPSLatitude]: piexif.GPSHelper.degToDmsRational(position.lat),
          [piexif.GPSIFD.GPSLongitudeRef]: position.lat < 0 ? 'S' : 'N',
          [piexif.GPSIFD.GPSLongitude]: piexif.GPSHelper.degToDmsRational(position.lon),
        }
      });
    }

    // Write thumbnail
    exifData = piexif.dump(metadata);
    exifImage = Buffer.from(piexif.insert(exifData, thumbnailBuffer.toString('binary')), 'binary');
    await fs.writeFile(resolve(thumbnailsPath, filename), exifImage);

    Object.assign(metadata, {
      thumbnail: thumbnailBuffer.toString('binary'),
      '1st': metadata['0th'],
      '0th': {
          [piexif.ImageIFD.XResolution]: imageMetadata.width,
          [piexif.ImageIFD.YResolution]: imageMetadata.height,
          [piexif.ImageIFD.Software]: softwareId
        }
    });

    exifData = piexif.dump(metadata);
    exifImage = Buffer.from(piexif.insert(exifData, image.toString('binary')), 'binary');
    await fs.writeFile(resolve(path, filename), exifImage);

    images.push(serverImage);
    server.newImage(serverImage);
  });
};

let debounce = null;
button.on('interrupt', (level) => {
  if (debounce === null) {
    if (position) {
      console.log('Took photo at', position.time, position.lat, position.lon);
    } else {
      console.log('Took photo at uncertain time/place', new Date());
    }
    debounce = Promise.all([
      new Promise((resolve) => {
        setTimeout(resolve, 1000);
      }),
      takeImage()
    ]).then(() => {
      debounce = null;
    });
  }
});

const checkWifi = () => exec(`iwconfig ${wifi}`).then((output) => {
  if (output.stdout.match(/ESSID:off/)) {
    console.log('Turning off WiFi as no connection');
    return exec(`iwconfig ${wifi} txpower off`);
  }
});

setTimeout(() => {
  console.log('Checking WiFi connection');
  checkWifi();
}, 60000);

console.log('Turning on Wifi');

exec(`iwconfig ${wifi} txpower auto`);

const checkDir = (path) => fs.stat(path).then((stat) => {
  if (!stat.isDirectory()) {
    return Promise.reject(new Error(`Path ${path} is not a directory`));
  }

  return fs.access(path, R_OK | W_OK);
})

checkDir(path).then(() => checkDir(thumbnailsPath).catch((error) => {
  if (error.code === 'ENOENT') {
    return fs.mkdir(thumbnailsPath);
  }

  return Promise.reject(error);
})).then(() => glob('*.jpg', { cwd: thumbnailsPath })).then((files) => {
  console.log('Ready for picking. Starting server...');
  return Promise.all(files.map(
    (file) => fs.readFile(join(thumbnailsPath, file)).then(
      (buffer) => piexif.load(buffer.toString('binary'))
    ).then((metadata) => ({
      src: file,
      width: metadata['1st'][piexif.ImageIFD.XResolution],
      height: metadata['1st'][piexif.ImageIFD.YResolution],
      time: metadata['Exif'][piexif.ExifIFD.DateTimeOriginal],
      position: metadata['GPS'][piexif.GPSIFD.GPSLatitude] ? {
        lat: piexif.GPSHelper.dmsRationalToDeg(
          metadata['GPS'][piexif.GPSIFD.GPSLatitude],
          metadata['GPS'][piexif.GPSIFD.GPSLatitudeRef]
        ),
        lon: piexif.GPSHelper.dmsRationalToDeg(
          metadata['GPS'][piexif.GPSIFD.GPSLongitude],
          metadata['GPS'][piexif.GPSIFD.GPSLongitudeRef]
        )
      } : null
    }))
  ));
}, (error) => {
  console.error('Error with directories: ' + error.message);
  return Promise.reject(error);
}).then((metadata) => {
  images = images.concat(metadata);
  server.start(thumbnailsPath, images);
  console.log('Ready!');
});

